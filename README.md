# Galactic

Galactic is a super tiny custom firmware package for Nintendo Switch. While it originally started as a Kosmos/DeepSea fork, it is now an independent package with a different focus. While Kosmos/DeepSea are meant to be all-in-one packages that make the process quick and easy, Galactic provides a small base installation that the user is expected to expand upon. An analogy can be drawn here for those familiar with GNU/Linux: Kosmos and DeepSea are the Ubuntu and we are Manjaro. A small package, but not so heavily user-centric that it alienates the average user. (*cough* ARCH LINUX *cough*)

Galactic only supports hekatmosphere at this time.

The latest supported Horizon version (Switch OS version) is **14.1.1**.

Packages are currently being made manually, but **work is being done** privately **to get Kosmos Builder refactored and/or altered to be compatible**. If you are interested in contributing to this builder tool (dubbed Nucleus by *moi*), send me a Matrix DM: `@fierymewtwo:matrix.org`. 

# Installation:

An up-to-date, easy to use installation guide is being worked on at [cfw.rf.gd](https://cfw.rf.gd). For now, until it's done, see the wiki for basic directions.



> Galactic Crew does not condone or support piracy. Sigpatches are included for the sole purpose of installing and playing games you bought on stock firmware or using forwarders!


Stay stellar
- sataa and crew ;)
